var tickEvent = new Event("timerTickEvent");
var finishEvent = new Event("timerFinishEvent")

function Timer(delay, repetitions) {
  this.delay = delay;
  this.maxrep = repetitions;
  this.rep = 0;
  this.started = false;
  this.event = document.createElement("event");

  this.start = function() {
    this.started = true;
    setTimeout(() => this.tick(), this.delay);
  }

  this.tick = function() {
    this.rep++;
    this.event.dispatchEvent(tickEvent);
    if (this.rep < this.maxrep) {
      setTimeout(() => this.tick(), this.delay);
    } else {
      this.rep = 0;
      this.started = false;
      this.event.dispatchEvent(finishEvent);
    }
  }
}