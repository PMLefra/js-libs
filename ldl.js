/*
 * v0.97.5
 * ldl: Lefra draw library
 * Simple & lightweight 2D HTML5 canvas drawing library, inspired by Processing & p5.js
 * Copyright (c) 2022 PMLefra.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3 <http://www.gnu.org/licenses/>.
*/

const PI = Math.PI,
	TAU = 2 * PI,
	RIGHT = 2,
	CENTER = 1,
	LEFT = 0,
	BOLD = "bold",
	CORNER = "corner",
	CORNERS = "corners",
	SPACEBAR = 32,
	RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
  UP_ARROW = 38,
  DOWN_ARROW = 40;

let windowWidth = window.innerWidth,
  windowHeight = window.innerHeight;

let canvas = document.createElement('canvas');
const ctx = canvas.getContext("2d");
canvas.id = "canvas";
canvas.tabIndex = 1;
canvas.style.margin = "0px";
canvas.style.padding = "0px";

let frameCount = 0;
let keyCode, mouseX = 0, mouseY = 0, mouseButton, mouseIsDown;
let width = 0, height = 0;

const ldl = {
  version: "0.97.4",
  states: {
    strokeOn: true,
    txtFont: "sans-serif",
    txtSize: 15,
    txtStyle: "",
    rectM: CORNER
  },
  pushStates: {}
}

ldl.pushStates = Object.assign({}, ldl.states);

textSize(ldl.states.txtSize);

let animVars = {
  startTime: null,
  now: null,
  then: null,
  elapsed: null,
  frameRate: 60,
  fpsInterval: 1000 / 60,
  doLoop: true,
  reqId: null
}

// Importing functions
const floor = Math.floor;
const round = Math.round;
const sin = Math.sin;
const cos = Math.cos;
const exp = Math.exp;
const abs = Math.abs;
const min = Math.min;
const max = Math.max;

/* Canvas management functions */

function createCanvas(w, h) {
	canvas.width = w;
  canvas.height = h;
  canvas.style.display = "block";
  width = w;
  height = h;
  document.body.prepend(canvas);
}

function resizeCanvas(w, h) {
  canvas.width = w;
  canvas.height = h;
  width = w;
  height = h;
}

/* Classes */

class Vector {
	x;
	y;
	z;

	constructor(x = 0, y = 0, z = 0) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	add(vector) {
		this.x += vector.x;
		this.y += vector.y;
		this.z += vector.z;
		return this;
	}

	sub(vector) {
		this.x -= vector.x;
		this.y -= vector.y;
		this.z -= vector.z;
		return this;
	}
	
	mult(coef) {
		this.x *= coef;
		this.y *= coef;
		this.z *= coef;
		return this;
	}

	div(coef) {
		this.x /= coef;
		this.y /= coef;
		this.z /= coef;
		return this;
	}

	// 2D vector only
	setMag(coef) {
		let r = coef / Vector.norm(this);
		this.x *= r;
		this.y *= r;
		return this;
	}

	round() {
	  this.x = Math.round(this.x);
	  this.y = Math.round(this.y);
	  this.z = Math.round(this.z);
	  return this;
	}

	static mult(vec, coef) {
		return new Vector(vec.x * coef, vec.y * coef, vec.z * coef);
	}

	static div(vec, coef) {
		return new Vector(vec.x / coef, vec.y / coef, vec.z / coef);
	}

	static add(vec1, vec2) {
		return new Vector(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
	}

	static sub(vec1, vec2) {
		return new Vector(vec1.x - vec2.x, vec1.y - vec2.y, vec1.z - vec2.z);
	}

	static addn(vec, n) {
		return new Vector(vec.x + n, vec.y + n, vec.z + n);	
	}

	static subn(vec, n) {
		return new Vector(vec.x - n, vec.y - n, vec.z - n);	
	}

	static dot(vec1, vec2) {
		return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
	}

	static norm(vec) {
		return Math.sqrt(Vector.dot(vec, vec));
	}
}

function createVector(x = 0, y = 0, z = 0) {
  return new Vector(x, y, z);
}

/* Utilities */

function dist(x, y, x2, y2) {
  if (typeof x == "object") return Math.sqrt((x.x - y.x) ** 2 + (x.y - y.y) ** 2);
  else return Math.sqrt((x2 - x) ** 2 + (y2 - y) ** 2);
}

function distSquared(x, y, x2, y2) {
  if (typeof x == "object") return (x.x - y.x) ** 2 + (x.y - y.y) ** 2;
  else return (x2 - x) ** 2 + (y2 - y) ** 2;
}

/**
 * Function that changes numbered properties over time, useful to animate things
 * @param {Object} obj - Objet contenant la propriété
 * @param {string} value - Nom de la propriété à modifier dans l'objet
 * @param {number} fValue - Valeur finale de la propriété
 * @param {number} time - Temps en ms
 * @param {function} callback - Fonction qui sera appelée à la fin de l'animation
 * @param {boolean} EXCEPTION
 */
function movePropertyTo(obj, value, fValue, time = 1000, callback = () => {}, EXCEPTION = false) {
  let isEqual = function(a, b) {
    return Math.abs(a - b) < 10 ** -4;
  }

  if (time >= 100 || (EXCEPTION && time >= 20)) {
    let iValue = obj[value];
    let iterationLength = Math.floor(time / 20); //20 ms est la durée de chaque itération.
    let deltaChange = (fValue - iValue) / iterationLength;
    let interval = setInterval(function() {
      if (!isEqual(obj[value], fValue)) {
        obj[value] += deltaChange;
      } else {
        obj[value] = fValue;
        clearInterval(interval);
        callback();
      }
    }, 20);
  } else {
    throw new Error("can't move a property in less than 100ms");
  }
}

function random(min, max) {
  if (typeof max != "undefined") return min + Math.round(Math.random() * (max - min));
  else return Math.round(Math.random() * min);
}

function constrain(el, min, max) {
  if (el < min) el = min;
  else if (el > max) el = max;
  return el;
}

function map(val, blow, bhigh, alow, ahigh) {
  let factor = (val - blow) / (bhigh - blow);
  return factor * (ahigh - alow) + alow;
}

function getUrlParam(param) {
  return new URL(window.location.href).searchParams.get(param);
}

/* Drawing functions */

/**
 * Turns an array of numbers describing a color into a string describing the same one
 * @param {number[]} array
 * @returns {string}
 */
function getColorTag(array) {
  let fstr = "";
  if (array.length === 3) {
    fstr += "#";
    array.forEach(function(a) {
      let fa = a.toString(16);
      if (fa.length === 1) fa = "0" + fa;
      fstr += fa;
    });
  } else if (array.length === 4) {
    fstr += "rgba(";
    fstr += array[0] + "," + array[1] + "," + array[2] + "," + array[3] + ")";
  }
  return fstr;
}

function fixHexColor(hexColor) {
  if (hexColor[0] === "#") hexColor = hexColor.substr(1);
  let fixed = parseInt(hexColor, 16);
  if (fixed != undefined && fixed >= 0 && fixed <= 16777215) {
    fixed = fixed.toString(16);
    while (fixed.length < 6) {
      fixed = "0" + fixed;
    }
    return "#" + fixed;
  } else return null;
}

function background(color, c2, c3, alpha) {
  c2 = (typeof c2 === 'undefined') ? color : c2;
  c3 = (typeof c3 === 'undefined') ? color : c3;
  alpha = (typeof alpha === 'undefined') ? 1 : alpha / 100;

  ctx.save();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  
  if (typeof color === "number") {
    if (alpha == 1) ctx.fillStyle = getColorTag([color, c2, c3])
    else ctx.fillStyle = getColorTag([color, c2, c3, alpha]);
  } else if (typeof color === "object") ctx.fillStyle = getColorTag(color);
  else if (typeof color === "string") ctx.fillStyle = color;

  ctx.fillRect(0, 0, width, height);
  ctx.restore();
}

function randomColor() {
  return [Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255)];
}

function rectMode(mode) {
  ldl.states.rectM = mode;
}

function rect(x, y, w, h) {
  if (ldl.states.rectM === CORNER) {
    ctx.fillRect(x, y, w, h);
    if (ldl.states.strokeOn) ctx.strokeRect(x + 0.5, y + 0.5, w, h);
  } else if (ldl.states.rectM === CENTER) {
    ctx.fillRect(x - w / 2, y - h / 2, w, h);
    if (ldl.states.strokeOn) ctx.strokeRect(x - w / 2 + 0.5, y - w / 2 + 0.5, w, h);
  } else if (ldl.states.rectM === CORNERS) {
    ctx.fillRect(x, y, w - x, h - y);
    if (ldl.states.strokeOn) ctx.strokeRect(x + 0.5, y + 0.5, w - x, h - y);
  }
}

function line(x, y, x2, y2) {
  ctx.beginPath();
  ctx.moveTo(x + 0.5, y + 0.5);
  ctx.lineTo(x2 + 0.5, y2 + 0.5);
  ctx.stroke();
}

// (x1, y1) and (x4, y4): anchor points
// (x2, y2) and (x3, y3): control points
function bezier(x1, y1, x2, y2, x3, y3, x4, y4) {
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.bezierCurveTo(x2, y2, x3, y3, x4, y4);
  ctx.stroke();
}

function point(x, y) {
  ctx.beginPath();
  ctx.ellipse(x, y, ctx.lineWidth / 2, ctx.lineWidth / 2, 0, 0, TAU);
  ctx.stroke();
}

function translate(x, y) {
  ctx.translate(x, y);
}

function stroke(color, c2, c3) {
  c2 = (typeof c2 === 'undefined') ? color : c2;
  c3 = (typeof c3 === 'undefined') ? color : c3;

  ldl.states.strokeOn = true;
  if (typeof(color) === "number") ctx.strokeStyle = getColorTag([color, c2, c3]);
  else if (typeof(color === "object")) ctx.strokeStyle = getColorTag(color);
}

function fill(color, c2, c3) {
  c2 = (typeof c2 === 'undefined') ? color : c2;
  c3 = (typeof c3 === 'undefined') ? color : c3;

  if (typeof(color) === "number") ctx.fillStyle = getColorTag([color, c2, c3]);
  else if (typeof(color) === "object") ctx.fillStyle = getColorTag(color);
  else if (typeof(color) === "string") ctx.fillStyle = color;
}

function noStroke() {
  ldl.states.strokeOn = false;
}

function strokeWeight(w) {
  ctx.lineWidth = w;
}

function noFill() {
  ctx.fillStyle = "rgba(255,255,255,0)";
}

function noContextMenu() {
  document.body.oncontextmenu = () => false;
}

function scale(n) {
  ctx.scale(n, n);
}

function ellipse(x, y, w, h) {
  ctx.beginPath();
  ctx.ellipse(x, y, w / 2, h / 2, 0, 0, TAU);
  ctx.fill();
  if (ldl.states.strokeOn) ctx.stroke();
}

function updateTextFont() {
	ctx.font = `${ldl.states.txtStyle} ${ldl.states.txtSize}px ${ldl.states.txtFont}`;
}

function textSize(size) {
  ldl.states.txtSize = size;
  updateTextFont();
}

function textFont(font) {
  ldl.states.txtFont = font;
  updateTextFont();
}

function textStyle(style) {
  if (style === BOLD) {
  	ldl.states.txtStyle = "bold";
  	updateTextFont();
  }
}

function getMaxFontDimensions() {
  let measure = ctx.measureText("█");
  return [measure.width, measure.fontBoundingBoxAscent + measure.fontBoundingBoxDescent];
}

function textWidth(text) {
  let measure = ctx.measureText(text);
  return measure.actualBoundingBoxRight - measure.actualBoundingBoxLeft;
}

function textHeight(text) {
  let measure = ctx.measureText(text);
  return measure.actualBoundingBoxAscent + measure.actualBoundingBoxDescent;
}

function textMonospace() {
  ldl.states.txtFont = "monospace";
  updateTextFont();
}

function rotate(angle) {
  ctx.rotate(angle);
}

function text(txt, x, y) {
  ctx.fillText(txt, x, y);
  if (ldl.states.strokeOn) ctx.strokeText(txt, x, y);
}

function loadImage(path) {
  let img = document.createElement("img");
  img.src = path;
  return img;
}

function image(img, x, y, w, h) {
  if (typeof w == "undefined") ctx.drawImage(img, x, y);
  else ctx.drawImage(img, x, y, w, h);
}

function push() {
  ctx.save();
  ldl.pushStates = Object.assign({}, ldl.states);
}

function pop() {
  ctx.restore();
  ldl.states = Object.assign({}, ldl.pushStates);
}

function color(str, c2, c3) {
  if (typeof str == "string") {
    let array = [];
    if (str[3] === 'a') {
      str = str.replace("rgba(", "");
      str = str.replace(")", "");
      array = str.split(",");
      for (let i = 0; i < array.length; i++) {
        array[i] = Number(array[i]);
      }
    } else if (str[3] === '(') {
      str = str.replace("rgb(", "");
      str = str.replace(")", "");
      array = str.split(",");
      for (let i = 0; i < array.length; i++) {
        array[i] = Number(array[i]);
      }
    }
    return array;
  } else if (typeof str == "number" && typeof c2 == "number" && typeof c3 == "number") {
    return [str, c2, c3];
  } else if (typeof str == "object") {
    return str;
  }
}

/* Events */

canvas.onclick = function(e) {
  mouseButton = e.button;

  if (typeof mousePressed === "function")
  	mousePressed(e);
  if (typeof mouseClicked === "function")
    mouseClicked(e);
};

canvas.oncontextmenu = function(e) {
  mouseButton = e.button;
  
  if (typeof mousePressed === "function")
    mousePressed(e);

  if (typeof mouseClicked === "function")
    mouseClicked(e);
}

canvas.onmousedown = function(e) {
  mouseButton = e.button;
  mouseIsDown = true;
  
  if (typeof mouseDown === "function")
    mouseDown(e);
};

canvas.onmouseup = function(e) {
  mouseButton = e.button;
  mouseIsDown = false;
  
  if (typeof mouseUp === "function")
    mouseUp(e);
};

canvas.onmousemove = function(e) {
	let rect = e.target.getBoundingClientRect();
  mouseX = e.clientX - rect.left;
  mouseY = e.clientY - rect.top;
  
  if (typeof mouseMoved === "function")
    mouseMoved(e);
};

canvas.onkeydown = function(e) {
  keyCode = e.keyCode;
  
  if (typeof keyPressed === "function")
    keyPressed(e);
};

canvas.onkeyup = function(e) {
  keyCode = e.keyCode;
  
  if (typeof keyReleased === "function")
    keyReleased(e);
};

canvas.onmousewheel = function(e) {
  e.delta = e.deltaY;
  
  if (typeof mouseWheel === "function")
    mouseWheel(e);
}

window.onresize = function(e) {
  windowWidth = window.innerWidth;
  windowHeight = window.innerHeight;
  
  if (typeof windowResized === "function")
    windowResized(e);
}

function frameRate(set) {
  if (set == 0) {
    stopAnimating();
  } else if (typeof set !== "undefined") {
    stopAnimating();
    startAnimating(Math.abs(Number(set)));
  } else {
    return constrain(animVars.frameRate, 0, 60);
  }
}

function startAnimating(fps) {
  animVars.doLoop = true;
  animVars.frameRate = fps;
  animVars.fpsInterval = 1000 / fps;
  animVars.then = Date.now();
  animVars.startTime = animVars.then;
  animate();
}

function stopAnimating() {
  cancelAnimationFrame(animVars.reqId);
  animVars.frameRate = 0;
  animVars.doLoop = false;
}

function animate() {
  if (animVars.doLoop) animVars.reqId = requestAnimationFrame(animate);

  frameCount++;
  animVars.now = Date.now();
  animVars.elapsed = animVars.now - animVars.then;

  // Drawing
  if (animVars.elapsed > animVars.fpsInterval) {
    animVars.then = animVars.now - (animVars.elapsed % animVars.fpsInterval);
    if (typeof draw === "function") draw();
    ctx.setTransform(1, 0, 0, 1, 0, 0);
  }
}

// Setup
window.onload = () => {
  if (typeof setup === "function") setup();
  if (typeof draw === "function" && animVars.doLoop) startAnimating(animVars.frameRate);
  canvas.focus();
};
