// Copyright LefraPM 2018
// @Lefra99M
// http://lefra.ovh

function Complex(reOrMod, imOrArg, type) {
  if (type == "alg" || type == undefined) {
    this.re = reOrMod;
    this.im = imOrArg;
    this.mod = Math.sqrt(this.re ** 2 + this.im ** 2);
    this.arg = Math.atan(this.im / this.re);
  } else if (type == "exp" || type == "trig") {
    this.mod = reOrMod;
    this.arg = imOrArg;
    this.re = this.mod * Math.cos(this.arg);
    this.im = this.mod * Math.sin(this.arg);
  }

  this.re = Math.round(this.re * 10 ** 9) / 10 ** 9;
  this.im = Math.round(this.im * 10 ** 9) / 10 ** 9;
  this.mod = Math.round(this.mod * 10 ** 9) / 10 ** 9;
  this.arg = Math.round(this.arg * 10 ** 9) / 10 ** 9;

  this.algExpr = "";
  if (this.im > 0) {
    if (this.re != 0) this.algExpr += this.re + "+";
    if (this.im == 1) this.algExpr += "i";
    else this.algExpr += "i*" + this.im;
  } else {
    if (this.re != 0) this.algExpr += this.re + "-";
    if (this.im == -1) this.algExpr += "i";
    else this.algExpr += "i*" + (-this.im);
  }

  this.expExpr = "";
  if (this.mod != 1) this.expExpr += this.mod + "*";
  this.expExpr += "exp(i*" + this.arg + ")";

  this.trigExpr = "";
  if (this.mod != 1) {
    this.trigExpr += this.mod;
    this.trigExpr += "*(cos(" + this.arg + ")+i*sin(" + this.arg + "))";
  } else {
    this.trigExpr = "cos(" + this.arg + ")+i*sin(" + this.arg + ")"
  }
}

//PROTOTYPE

Complex.prototype.toString = function() {
  return this.algExpr;
}

Complex.prototype.toExp = function() {
  return this.expExpr;
}

Complex.prototype.toTrig = function() {
  return this.trigExpr;
}

//INSTANCIATION
function c(reOrMod, imOrArg, type) {
  if ((type == "alg" || type == undefined) && imOrArg == 0) return reOrMod;
  else if (type == "exp" || type == "trig") {
    if (imOrArg % (2 * Math.PI) == 0) return reOrMod;
    else if (imOrArg % Math.PI == 0) return -reOrMod;
  }
  return new Complex(reOrMod, imOrArg, type);
}

//REAL FUNCTIONS
Complex.sqrt = function(x) {
  if (x >= 0) {
    return Math.sqrt(x);
  } else {
    return c(0, Math.sqrt(-x));
  }
}

//COMPLEX FUNCTIONS
Complex.add = function(z1, z2) {
  if (typeof z1 == "number" && typeof z2 == "number") return z1 + z2;
  if (typeof z1 != "number" && typeof z2 == "number") return c(z1.re + z2, z1.im);
  if (typeof z1 == "number" && typeof z2 != "number") return c(z1 + z2.re, z2.im);
  else return c(z1.re + z2.re, z1.im + z2.im);
}

Complex.sub = function(z1, z2) {
  if (typeof z1 == "number" && typeof z2 == "number") return z1 - z2;
  if (typeof z1 != "number" && typeof z2 == "number") return c(z1.re - z2, z1.im);
  if (typeof z1 == "number" && typeof z2 != "number") return c(z1 - z2.re, z2.im);
  else return c(z1.re - z2.re, z1.im - z2.im);
}

Complex.mult = function(z1, z2) {
  if (typeof z1 == "number" && typeof z2 == "number") return z1 * z2;
  if (typeof z1 != "number" && typeof z2 == "number") return c(z1.re * z2, z1.im * z2);
  if (typeof z1 == "number" && typeof z2 != "number") return c(z1 * z2.re, z1 * z2.im);
  else return c(z1.re * z2.re - z1.im * z2.im, z1.re * z2.im + z1.im * z2.re);
}

Complex.divide = function(z1, z2) {
  if (typeof z1 == "number" && typeof z2 == "number") return z1 / z2;
  if (typeof z1 != "number" && typeof z2 == "number") return c(z1.re / z2, z1.im / z2);
  if (typeof z1 == "number" && typeof z2 != "number") return c(z1 / z2.re, z1 / z2.im);
  else return c((z1.re * z2.re + z1.im * z2.im) / (z2.re ** 2 + z2.im ** 2), (z1.im * z2.re - z1.re * z2.im) / (z2.re ** 2 + z2.im ** 2));
}

Complex.square = function(z) {
  if (typeof z == "number") return z ** 2
  else return c(z.re ** 2 - z.im ** 2, 2 * z.re * z.im);
}

Complex.pow = function(z, p) {
  if (typeof z == "number") return Math.pow(z, p);
  else {
    let res = z;
    for (let i = 1; i < p; i++) {
      res = Complex.mult(res, z);
    }
    return res;
  }
}

Complex.exp = function(z) {
  if (typeof z == "number") return Math.exp(z);
  else return c(Math.exp(z.re), z.im, "exp");
}

Complex.re = function(z) {
  if (typeof z == "number") return z;
  else return z.re;
}

Complex.im = function(z) {
  if (typeof z == "number") return 0;
  else return z.im;
}

Complex.mod = function(z) {
  if (typeof z == "number") return Math.abs(z);
  else return z.mod;
}

Complex.arg = function(z) {
  if (typeof z == "number") return 0;
  else return z.arg;
}

Complex.conj = function(z) {
  if (typeof z == "number") return z;
  else return c(z.re, -z.im);
}

Complex.areEqual = function(z1, z2) {
  if (typeof z1 == "number" && typeof z2 == "number") return z1 == z2;
  if (typeof z1 != "number" && typeof z2 == "number") return (z1.re == z2 && z1.im == 0);
  if (typeof z1 == "number" && typeof z2 != "number") return (z1 == z2.re && z2.im == 0);
  else return (z1.re == z2.re && z1.im == z2.im);
}